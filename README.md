# Liste d'outils et de plateformes Libre et open source pour créer, gérer des applications mobile et web en nocode, lowcode

Juste une liste d'applications libre et open source de nocode, lowcode | Just a Free/Libre Open Source Software nocode-Lowcode application list.

> _Merci de garder en mémoire la cible : Des outils et plateformes du monde du **logiciel libre et open source** pour réaliser des applications web et mobile en **nocode et lowcode**._

> _Please keep in mind the target: **free and open source software** tools and platforms to develop web and mobile applications in **nocode and lowcode**._

# La liste | The list

## Développements d’applications mobiles et/ou web

- **MIT App Inventor 2** http://www.appinventor.mit.edu/
- Merci @AnotherITcorp qui m'a signalé que **Thunkable** n'était pas FLOSS, contrairement à ce que j'avais annoncé ! Détails ci-dessous.
- **Silex** (https://www.silex.me/)
- **Convertigo** (Low-Code / No-Code , Open Source Web & mobile application development platform - Convertigo)
- **Infinable** (https://infinable.io/)
- **Saltcorn** (https://github.com/saltcorn/saltcorn)
- **Android Studio** (Download Android Studio and SDK tools  |  Android Developers) Open source et produit Google
- **Supabase** (https://supabase.io/) *** alternative OS à Firebase
- **NHOST** https://www.nhost.io/ Nhost is a Google Firebase alternative but with SQL, GraphQL and no vendor lock-in. Beta.
- **Tooljet** https://tooljet.com/ Open-source low-code framework to build & deploy internal tools within minutes. @fox
- **BudiBase** https://budibase.com/ Create business apps in minutes @fox
- **OhMyForm**! https://ohmyform.com/ Craft beautiful forms in seconds
- **TellForm**. An opensource alternative to TypeForm. Deprecated. See OhMyForm.
- **SingleLink** The open-source micro-site platform,Singlelink. https://singlelink.co. LinkTree alternative
- **MY Template** https://mytemplate.xyz A no-code personal website builder for developers !!! (Les points d'exclamations sont moi :) ) En fait, génère une page html.
- **Webstudio** (https://webstudio.is) `Finally, an open source visual development tool!`. I just discovered it on ProductHunt :).
- vos propositins (FLOSS), …

## Applications for mobile and web apps : Base de données :

- **NocoDB** : (https://www.nocodb.com/)
- **Baserow** (https://baserow.io/) *** Merci à @Nathaniel
- **Directus** (https://directus.io/) " An Instant App & API for your SQL Database." *** merci @tcit
- **BaseTool** https://www.basetool.io/ View and manage all your data in one place like a pro @fox 
- vos propositins (FLOSS), …

## Applications for mobile and web apps : API/workflow :

- **n8n** (https://n8n.io/)
- **Beehive** (https://github.com/muesli/beehive)
- **DataFire** (framework) https://github.com/DataFire/DataFire
- **Huginn** (https://github.com/huginn/huginn)
- **HASURA** (https://hasura.io/) Citation de @manutopik : `... c’est pas du nocode de haut niveau, mais plutôt bas niveau. Hasura permet de créer une api REST et graphQL sans une ligne de code, avec gestion authentification et autorisation row/column level… et de faire des trucs géniaux comme merger des apis, du temps réel, des actions customs, trigger events…`
- **Tooljet** (https://tooljet.com) Open-source low-code framework to build & deploy internal tools within minutes.
- **_testsigma_** (https://testsigma.com/) : Open source test automation platform. Permet de lancer des tests d'applications sans savoir coder :)
- **Node Red** (https://nodered.org/) `Low-code programming for event-driven applications`. `Node-RED is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways.`. Signalé par @McFly sur Framacolibri :).
- **PipeDream** (https://pipedream.com/) `Stop writing boilerplate code, struggling with authentication and managing infrastructure. Start connecting APIs with code-level control when you need it — and no code when you don't`. Source : https://www.opensourcealternative.to/project/Pipedream. 
- vos propositins (FLOSS), …


## À étudier / confirmer :) | To be validated

- **AppSmith** (https://www.appsmith.com/) A powerful open source framework to build internal tools. Merci à Ambroise D.

- **LocoKit**  https://locokit.io/ https://github.com/locokit/locokit The Low Code Kit platform.  (Erreur de lien sur la première référence. Le lien vers LocoKit est désormais à jour)


## En amont de vos projets no-de, des outils de prototypage open source : croquis, Wireframing, mockup

**Important. Alex, de Contournement, m'a signalé que ces outils ne sont pas, stricto sensu, des outils de no-code. Et c'est vrai, je partage cette idée. J'ai voulu les citer pour, (1) en faire la promotion :), (2) parce que ces outils ce sont (souvent) utilisés en amont d'un projet no-code.
Du coup, je change le titre et je mets le chapitre vers la fin :).**

- Pencil Projet (https://pencil.evolus.vn)
- Penpot https://penpot.app/) (Ex-OpenBot)
- Figma-Linux (https://github.com/Figma-Linux/figma-linux) que je ne connais pas.
        Se présente : Figma-linux is an unofficial Electron-based Figma desktop app for Linux.
- vos propositins (FLOSS), …


## Commentaires additionnels | Some additional Commentaires

- Draftbit et Expo. Draftbit (https://draftbit.com) a été un temps dans cette liste. Draftbit est une application "propriétaire" et il est mentionné "that code is based on open-source frameworks and libraries". C'est sur **Expo** (https://expo.dev/) qu'il est basé , un framework pour le développement d'applications en React. Expo, n'est donc pas une Nocode / Lowcode application. Bien que FLOSS, il ne figure donc pas ici :).
        ~~Je ne suis pas sûr de bien voir les choses. Drafit semble « propriétaire ». Et il s’appuie sur un Framework open source.~~ 

- **SeaTable** est « libre » d’usage, mais pas dans le code source. *** Merci @fox. Je l'ai retiré de la liste (jrd10). Je laisse le commentaire ci-dessous.
        - SeaTable (SeaTable - simple comme Excel, puissant comme une base de données) « Il ressemble à Excel, mais a tellement plus à offrir. » *** merci @tcit

- L’espace Community de Figma https://www.figma.com/community (Figma est propriétaire). Son espace Communauté permet la création de projets libres et open source).

- _Merci à @AnotherITcorp qui m'a fait découvrir que **Thunkable** n'était pas FLOSS ! L'automatisation de Thunkable est basée sur AppInventor qui est FLOSS (voir ci-dessus), mais Thunkable n'y est pas !  https://thunkable.com/

# Quelques références | Some references

1. https://nocode-france.fr (focalise sur le nocode/low code, pas sur le FLOSS)
2. Rediffusion d'une vidéoconférence à Open Source Expo 2021 : `Panorama des outils nocode / low-code, alternatives à AirTable` https://www.youtube.com/watch?v=7erMuOHWhnk de Mathieu Dartigues (@fox)
3. Liste (FR, En, others) : https://www.btw.so/fr/open-source-alternatives
4. No-code and more. https://opensource.builders/


## Origine et contributions | Source and contributions

Cette page a été initiée suite à un fil sur Framacolibri : https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260/16

This page has created following this thread (in French): https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260/16

N'hésitez pas à proposer vos références | Fell free to propose your references

## Licence
Dans le domaine public CC0 1.0 | CC0 1.0 Public Domain

## Situation du projet | Project status
C'est un projet ouvert qui peut être rejoint par toute personne intéressées sans prosélytisme excessif :) | This project is opten to all person interessed by the topic, without undue proselytizing :).


### Mise à jour 24 avril 2022 | Update April 2022

> Si vous souhaitez excercer un droit de retrait ou de correction sur un ou des éléments dont vous êtes propriétaire ou dont vous exercez une certaine expertise, n'hésiter à me contacter à travers cette plateforme ou ici : [Forum Colibri](https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260).

> If you wish to exercise a right of withdrawal or correction on one or several elements of which you are the owner or of which you exercise a certain expertise, do not hesitate to contact me through this platform or here (in French) : [Forum Colibri](https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260).

- Traduction assistée avec https://www.deepl.com | Assisted translation with https://www.deepl.com
